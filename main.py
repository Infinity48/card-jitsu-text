from player import *

def game_tick(player1:Player, player2:Player) -> Player:
    player1.fill_hand()

    for i, card in enumerate(player1.hand):
        print(str(i) + ")", card)
    
    player1.use_card(int(input("What card? ")))
    print("Your card is", player1.choosen_card)
    player1.send_info()

    player2.sync(player1.connection)
    print("The other player's card is", player2.choosen_card, end="\n\n")

    p1_card = player1.choosen_card
    p2_card = player2.choosen_card

    if p1_card.can_win(p2_card):
        print("Your card beats the other player's!\n")
        player1.winnings[p1_card.element].append(p1_card)
    elif p2_card.can_win(p1_card):
        print("The other player's card beats yours!\n")
        player2.winnings[p2_card.element].append(p2_card)
    else:
        print("Tie!\n")
    
    string = "Fire: {0}\nWater: {1}\nSnow: {2}"
    info = []
    for elem in player1.winnings.keys():
            eleminf = []
            for card in player1.winnings[elem]:
                eleminf.append(card.colour)
            info.append(eleminf)
    string = string.format(info[0], info[1], info[2])
    print("Your winnings:\n" + string, end="\n\n")

    string = "Fire: {0}\nWater: {1}\nSnow: {2}"
    info = []
    for elem in player2.winnings.keys():
            eleminf = []
            for card in player2.winnings[elem]:
                eleminf.append(card.colour)
            info.append(eleminf)
    string = string.format(info[0], info[1], info[2])
    print("The other player's winnings:\n" + string, end="\n\n")

    if player1.has_won():
        return player1
    elif player2.has_won():
        return player2
    else:
        return None

def main():
    server = input("Server? (y/N) ").lower() == "y"
    if server:
        port = input("Server Port (10000)? ")
        if port == "":
            port = 10000
        else:
            port = int(port)
        p1 = PlayerServer(port)
    else:
        ip = input("Server Name/IP? ")
        port = input("Server Port (10000)? ")
        if port == "":
            port = 10000
        else:
            port = int(port)
        p1 = PlayerClient(ip, port)
    p2 = PlayerPuppet()
    winner = None
    
    while not winner:
        winner = game_tick(p1, p2)

    if winner == p1:
        print("You won!")
    elif winner == p2:
        print("You lost!")
    input()

if __name__ == "__main__":
    main()
