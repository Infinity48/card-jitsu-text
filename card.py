from dataclasses import dataclass
from consts import *

@dataclass
class Card:
    element: str
    level: int
    colour: str

    def __repr__(self) -> str:
        return "{colour} {element} card of level {level}".format(colour=self.colour, element=self.element, level=self.level)
    
    @classmethod
    def from_dict(cls, card_dict):
        return cls(card_dict["element"], card_dict["level"], card_dict["colour"])

    def can_win(self, other) -> bool:
        if self.element == ELEM_FIRE and other.element == ELEM_SNOW:
            return True
        elif self.element == ELEM_WATER and other.element == ELEM_FIRE:
            return True
        elif self.element == ELEM_SNOW and other.element == ELEM_WATER:
            return True
        elif self.element == other.element and self.level > other.level:
            return True
        else:
            return False
