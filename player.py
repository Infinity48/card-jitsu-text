from card import *
from consts import *
from random import randint, choice
import socket, json

class Player:
    def __init__(self):
        self.choosen_card:Card = None
        self.winnings = {"fire": [], "water": [], "snow": []}
        self.hand = [None, None, None, None, None]
        self.socket = None
        self.connection = None
    
    def __repr__(self) -> str:
        return f"Drawn card: {self.choosen_card}"

    def fill_hand(self):
        for i in range(5):
            if self.hand[i] == None:
                self.hand[i] = Card(choice(ELEMS), randint(2, 12), choice(COLS))
    
    def use_card(self, index):
        self.choosen_card = self.hand[index]
        self.hand[index] = None

    def pick_random_card(self):
        index = randint(0, len(self.hand)-1)
        self.use_card(index)
    
    def has_won(self) -> bool:
        """ You win if you scored three cards of the same element but different colours or 
            have three cards of different colors and elements.
        """
        elem_cols = {"fire": set(), "water": set(), "snow": set()}
        for elem in self.winnings.keys():
            seen_cols = set()
            for card in self.winnings[elem]:
                seen_cols.add(card.colour)
                # Has three cards of same element but different colours?
                if len(seen_cols) >= 3:
                    return True
            elem_cols[elem] = seen_cols
        
        # Has three cards of different elements and different colours?
        if (elem_cols["fire"].difference(elem_cols["water"], elem_cols["snow"]) and
            elem_cols["snow"].difference(elem_cols["water"], elem_cols["fire"]) and
            elem_cols["water"].difference(elem_cols["fire"], elem_cols["snow"])):
                return True
        
        return False # The game continues...
    
    def send_info(self):
        pass

    def sync(self, sock):
        pass

class PlayerClient(Player):
    def __init__(self, ip, port):
        super().__init__()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((ip, port))
        self.connection = self.socket
        
    def send_info(self):
        wins = {}
        for key in self.winnings.keys():
            value = self.winnings[key]
            cards = []
            for card in value:
                cards.append(card.__dict__)
            wins[key] = cards
        data = {"choosen_card": self.choosen_card.__dict__, "winnings": wins}
        self.socket.sendall(json.dumps(data).encode("utf-8"))

class PlayerServer(Player):
    def __init__(self, port):
        super().__init__()
        host = socket.gethostname()
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((host, port))

        print(host, port)
        s.listen(1)
        conn, addr = s.accept()
        print('Connected by', addr)
        self.socket = s
        self.connection = conn
    
    def send_info(self):
        wins = {}
        for key in self.winnings.keys():
            value = self.winnings[key]
            cards = []
            for card in value:
                cards.append(card.__dict__)
            wins[key] = cards
        data = {"choosen_card": self.choosen_card.__dict__, "winnings": wins}
        self.connection.sendall(json.dumps(data).encode("utf-8"))

class PlayerPuppet(Player):
    def sync(self, sock):
        raw_data = sock.recv(1024)
        data = json.loads(raw_data.decode("utf-8"))
        
        self.choosen_card = Card.from_dict(data["choosen_card"])
        self.winnings = {"fire": [], "water": [], "snow": []}
        for elem in data["winnings"].keys():
            for card in data["winnings"][elem]:
                self.winnings[elem].append(Card.from_dict(card))
